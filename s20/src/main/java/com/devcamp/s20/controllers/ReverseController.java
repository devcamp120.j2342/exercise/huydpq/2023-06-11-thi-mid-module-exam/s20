package com.devcamp.s20.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ReverseController {
    @GetMapping("/reverse")
    public String reverseString(@RequestParam String input) {
        StringBuilder sb = new StringBuilder(input);
        return sb.reverse().toString();
    }
}
