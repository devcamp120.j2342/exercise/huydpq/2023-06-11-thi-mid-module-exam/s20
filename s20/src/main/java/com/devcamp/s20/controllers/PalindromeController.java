package com.devcamp.s20.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class PalindromeController {
    @GetMapping("/palindrome")
    public String checkPalindrome(@RequestParam String input) {
        String reversed = new StringBuilder(input).reverse().toString();
        if (input.equals(reversed)) {
            return "This is a palindrome string.";
        } else {
            return "This isn't a palindrome string.";
        }
    }
}
